[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Preprocessing for anatomical data

Convert the original DICOM recordings into nifty format

## Notes

- The zipping of DICOM files happens in the script in the task folder.
- should be replaced with BIDS standard routine