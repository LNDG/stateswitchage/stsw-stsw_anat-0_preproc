#!/bin/bash

cd /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/A_scripts/A1_dcm2nii/

IDs=(1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2142 2253 2254 2255)

mkdir /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/Y_log/A1_dcm2nii

for i in $(seq 1 ${#IDs[@]}); do
for f in $(seq 1 2); do
	echo "#PBS -N dcm2nii_anat_${f}_${i}" 											> job
	echo "#PBS -l walltime=03:00:00" 												>> job
	echo "#PBS -l mem=8gb" 															>> job
	echo "#PBS -j oe" 																>> job
	echo "#PBS -o /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/Y_log/A1_dcm2nii" 	>> job
	echo "#PBS -m n" 																>> job
	echo "#PBS -d ." 																>> job
	echo "./A1_dcm2nii_run.sh /opt/matlab/R2016b $f $i " 							>> job
	qsub job
	rm job
done
done
