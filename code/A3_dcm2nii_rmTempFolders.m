function A3_dcm2nii_rmTempFolders

% Copy NII files into superdirectory.
% If there is an empty temp folder, remove it along with its contents.

% 180109 | written by JQK

if ismac
    pn.root         = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_anat/preproc/';
    pn.dcm2nii      = [pn.root, 'D_tools/dicm2nii']; addpath(genpath(pn.dcm2nii));

else
    pn.root         = '/home/mpib/LNDG/StateSwitch/WIP_anat/preproc/';
end

%% IDs

% N = 48 (180102);
IDList = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2142';'2253';'2254';'2255'};

% OA: N = 57 (180206);
IDList = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2142';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2253';'2254';'2255';'2258';'2261'};

fileTypes = {'T1'; 'T2'};
fileFolders = {'anat'; 'anat'};
fileNames = {'T1w'; 'T2w'};

% Note: 
% For subject 1126, only the first half was acquired, T2, REST and DTI are missing.
% For subjects 1268 and 1270, T1 and task data are from repeated session
% due to scanner crash during the first MR session.
secondSessionTypes = {'REST'; 'T2'; 'DTI_6'; 'DTI_64'};

for indType = 1:2
    for indID = 1:numel(IDList)
        disp(['Removing tmp folders: ',fileTypes{indType}, ' Subject ' IDList{indID}]);

        % For subject 1126, only the first half was acquired, T2, REST and DTI are missing.
        if ~(max(strcmp(fileTypes{indType}, secondSessionTypes))==1 & strcmp(IDList{indID}, '1126'))
            % designate paths
            fileName = ['sub-STSWD', IDList{indID}, '_' fileNames{indType}];
            pn.target = [pn.root, 'B_data/B_nii/', fileName];
            if exist(pn.target) && strcmp(fileNames{indType},'T1w')
                fileToCopy = [pn.target, '/t1_mprage_sag_MD_1mm.nii.gz'];
                fileTarget = [pn.target, '.nii.gz'];
                movefile(fileToCopy, fileTarget);
                
                fileToCopy = [pn.target, '/t1_mprage_sag_MD_1mm.json'];
                fileTarget = [pn.target, '.json'];
                if exist(fileToCopy)
                    movefile(fileToCopy, fileTarget);
                end

                fileToCopy = [pn.target, '/dcmHeaders.mat'];
                fileTarget = [pn.target, '_hdr.mat'];
                if exist(fileToCopy)
                    movefile(fileToCopy, fileTarget);
                end

                rmdir(pn.target, 's');
                
            elseif exist(pn.target) && strcmp(fileNames{indType},'T2w')
                fileToCopy = [pn.target, '/t2_spc_ns_sag_p2_iso.nii.gz'];
                fileTarget = [pn.target, '.nii.gz'];
                movefile(fileToCopy, fileTarget);
                
                fileToCopy = [pn.target, '/t2_spc_ns_sag_p2_iso.json'];
                fileTarget = [pn.target, '.json'];
                if exist(fileToCopy)
                    movefile(fileToCopy, fileTarget);
                end

                fileToCopy = [pn.target, '/dcmHeaders.mat'];
                fileTarget = [pn.target, '_hdr.mat'];
                if exist(fileToCopy)
                    movefile(fileToCopy, fileTarget);
                end

                rmdir(pn.target, 's');
                
            end
        end
    end
end

end % A1_dcm2nii