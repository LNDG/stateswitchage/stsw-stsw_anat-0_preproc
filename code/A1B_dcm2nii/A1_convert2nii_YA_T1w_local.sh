#!/bin/bash

#SubjectID='1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2142 2253 2254 2255'

SubjectID='1215'

pn_root='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/anat/preproc'

for SID in ${SubjectID}; do
	
	cd ${pn_root}/B_data/A_dicom
	mkdir ${SID}
	unzip ${pn_root}/B_data/A_dicom/sub-STSWD${SID}_T1w.zip -d ${pn_root}/B_data/A_dicom/${SID}

	cd ${pn_root}/B_data/A_dicom/${SID}/*
	
	${pn_root}/D_tools/dcm2nii -f *
	
	mkdir ${pn_root}/B_data/C_t1/${SID}
	mv 2*.nii.gz ${pn_root}/B_data/C_t1/${SID}
	mv co*.nii.gz ${pn_root}/B_data/C_t1/${SID}
	mv o*.nii.gz ${pn_root}/B_data/C_t1/${SID}
	
	rm -rf ${pn_root}/B_data/A_dicom/${SID}
	
done