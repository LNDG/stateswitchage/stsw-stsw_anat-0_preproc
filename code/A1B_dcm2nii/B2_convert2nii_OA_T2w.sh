#!/bin/bash

SubjectID='2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261'

pn_root='/home/mpib/LNDG/StateSwitch/WIP_anat/preproc'

for SID in ${SubjectID}; do
	
	echo "#PBS -N dcm2nii_T2w_${SID}" 						>> job # Job name 
	echo "#PBS -l walltime=0:10:00" 									>> job # Time until job is killed 
	echo "#PBS -l mem=1gb" 												>> job # Books 4gb RAM for the job 
	echo "#PBS -m n" 													>> job # Email notification on abort/end, use 'n' for no notification 
	echo "#PBS -o ${pn_root}/Y_log/A1_dcm2nii" 										>> job # Write (output) log to group log folder 
	echo "#PBS -e ${pn_root}/Y_log/A1_dcm2nii" 										>> job # Write (error) log to group log folder 
	
	echo "cd ${pn_root}/B_data/A_dicom" 	>> job
	echo "mkdir ${SID}">> job
	echo "unzip ${pn_root}/B_data/A_dicom/sub-STSWD${SID}_T2w.zip -d ${pn_root}/B_data/A_dicom/${SID}" 	>> job

	echo "cd ${pn_root}/B_data/A_dicom/${SID}/*" 	>> job
	
	echo "${pn_root}/D_tools/dcm2nii -x Y -f *" 	>> job
	
	echo "mkdir ${pn_root}/B_data/D_t2/${SID}" 	>> job
	echo "mv 2*.nii.gz ${pn_root}/B_data/D_t2/${SID}" 	>> job
	echo "mv co*.nii.gz ${pn_root}/B_data/D_t2/${SID}" 	>> job
	echo "mv o*.nii.gz ${pn_root}/B_data/D_t2/${SID}" 	>> job
	
	echo "rm -rf ${pn_root}/B_data/A_dicom/${SID}" 	>> job

	qsub job
	rm job
	
done