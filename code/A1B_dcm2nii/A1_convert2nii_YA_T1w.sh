#!/bin/bash

#SubjectID='1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2142 2253 2254 2255'

SubjectID='1215'

pn_root='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/anat/preproc'

for SID in ${SubjectID}; do
	
	echo "#PBS -N dcm2nii_${SID}" 						>> job # Job name 
	echo "#PBS -l walltime=0:10:00" 					>> job # Time until job is killed 
	echo "#PBS -l mem=1gb" 								>> job # Books 4gb RAM for the job 
	echo "#PBS -m n" 									>> job # Email notification on abort/end, use 'n' for no notification 
	echo "#PBS -o ${pn_root}/Y_log/A1_dcm2nii" 			>> job # Write (output) log to group log folder 
	echo "#PBS -e ${pn_root}/Y_log/A1_dcm2nii" 			>> job # Write (error) log to group log folder 
	
	echo "cd ${pn_root}/B_data/A_dicom" 				>> job
	echo "mkdir ${SID}"									>> job
	echo "unzip ${pn_root}/B_data/A_dicom/sub-STSWD${SID}_T1w.zip -d ${pn_root}/B_data/A_dicom/${SID}" 	>> job

	echo "cd ${pn_root}/B_data/A_dicom/${SID}/*" 		>> job
	
	echo "${pn_root}/D_tools/dcm2nii -f *" 				>> job
	
	echo "mkdir ${pn_root}/B_data/C_t1/${SID}" 			>> job
	echo "mv 2*.nii.gz ${pn_root}/B_data/C_t1/${SID}" 	>> job
	echo "mv co*.nii.gz ${pn_root}/B_data/C_t1/${SID}" 	>> job
	echo "mv o*.nii.gz ${pn_root}/B_data/C_t1/${SID}" 	>> job
	
	echo "rm -rf ${pn_root}/B_data/A_dicom/${SID}" 		>> job

	qsub job
	rm job
	
done